import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CustomNgPrimeModule } from './ngprime/custom.ngprime.module';
import { CustomBootstrapModule } from './bootstrap/custom.bootstrap.module';
import { CallbackComponent } from './callback/callback.component';

@NgModule({
  declarations: [
    CallbackComponent
  ],
  imports: [
    RouterModule.forRoot([])
  ],
  exports: [
    CustomBootstrapModule, CustomNgPrimeModule
  ]
})
export class SharedModule { }
